package com.khunpon.week11;

public class Bird extends Animal implements Flyable,Walkable{

    public Bird(String name) {
        super(name, 2);

    }

    @Override
    public void sleep() {
        System.out.println(this.toString() + "sleep.");
    }

    @Override
    public void eat() {
        System.out.println(this.toString() + "eat.");
    }

    @Override
    public String toString() {
        return "Bird(" + this.getName() + ")";
    }

    @Override
    public void takeoff() {
        System.out.println(this.toString() + "takeoff.");
    }

    @Override
    public Void fly(){
        System.out.println(this.toString() + "fly.");
        return null;

    }
    @Override
    public void landing() {
        System.out.println(this.toString() + "landing.");
        
    }

    @Override
    public void walk() {
        System.out.println(this + "Walk.");

    }

    @Override
    public void run() {
        System.out.println(this + "run.");

    }

}
