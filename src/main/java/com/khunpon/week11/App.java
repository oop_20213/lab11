package com.khunpon.week11;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        Bird bird1 = new Bird("Tweety");
        bird1.eat();
        bird1.sleep();
        bird1.takeoff();
        bird1.fly();
        bird1.landing();
        bird1.walk();
        Plane boeing = new Plane("Boeing", "Rosaroi");
        boeing.takeoff();
        boeing.fly();
        boeing.landing();
        Superman clark = new Superman("Clark");
        clark.takeoff();
        clark.fly();
        clark.landing();
        clark.walk();
        clark.run();
        clark.eat();
        clark.sleep();
        clark.swim();
        Human man1 = new Human("Man");
        man1.walk();
        man1.run();
        man1.eat();
        man1.sleep();
        man1.swim();
        Bat batman = new Bat("Batman");
        bird1.eat();
        bird1.sleep();
        bird1.takeoff();
        bird1.fly();
        bird1.landing();
        Snake yellow = new Snake("yellow");
        yellow.eat();
        yellow.sleep();
        yellow.reptile();
        yellow.swim();
        Dog bobo = new Dog("bobo");
        bobo.run();
        bobo.walk();
        bobo.eat();
        bobo.sleep();
        Cat garfield = new Cat("garfield");
        garfield.walk();
        garfield.run();
        garfield.eat();
        garfield.sleep();
        Fish tong = new Fish("tong");
        tong.eat();
        tong.sleep();
        tong.swim();
        Crocodile kea = new Crocodile("kea");
        kea.reptile();
        kea.swim();
        kea.eat();
        kea.sleep();
        Rat jerry = new Rat("jerry");
        jerry.run();
        jerry.walk();
        jerry.eat();
        jerry.sleep();


        Flyable[] flyables = { bird1, boeing, clark, batman };
        for (int i = 0; i < flyables.length; i++) {
            flyables[i].takeoff();
            flyables[i].fly();
            flyables[i].landing();
        }
        Walkable[] walkables = { bird1, clark, man1, bobo, garfield , jerry};
        for (int i = 0; i < walkables.length; i++) {
            walkables[i].walk();
            walkables[i].run();
        }
        Swimable[] Swimable = { man1, clark, bobo, yellow, tong , kea };
        for (int i = 0; i < Swimable.length; i++) {
            Swimable[i].swim();

        }
        Reptilelable[] Reptilelable = { yellow, kea };
        for (int i = 0; i < Reptilelable.length; i++) {
            Reptilelable[i].reptile();
        }
    }
}
