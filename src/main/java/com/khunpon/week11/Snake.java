package com.khunpon.week11;

public class Snake extends Animal implements Reptilelable,Swimable {

    public Snake(String name) {
        super(name,0);
    }

    @Override
    public void reptile() {
        System.out.println(this.toString() + "reptile.");
        
    }

    @Override
    public void sleep() {
        System.out.println(this.toString() + "sleep.");
    }

    @Override
    public void eat() {
        System.out.println(this.toString() + "eat.");
    }
    @Override
    public String toString() {
        return "Snake(" + this.getName() + ")";
    }

    @Override
    public void swim() {
        System.out.println(this.toString() + "swim.");
        
    }
    
}
