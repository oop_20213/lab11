package com.khunpon.week11;

public class Crocodile extends Animal implements Reptilelable,Swimable {

    public Crocodile(String name) {
        super(name, 4);
    
    }

    @Override
    public void swim() {
        System.out.println(this + "Swim.");
        
    }

    @Override
    public void sleep() {
        System.out.println(this.toString() + "sleep.");
    }

    @Override
    public void eat() {
        System.out.println(this.toString() + "eat.");
    }

    @Override
    public void reptile() {
        System.out.println(this.toString() + "reptile.");
        
    }
    @Override
    public String toString() {
        return "Crocodile(" + this.getName() + ")";
    }
    
}
