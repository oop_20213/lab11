package com.khunpon.week11;

public interface Flyable {
    public void takeoff();
    public Void fly();
    public void landing();
}
