package com.khunpon.week11;

public class Bat extends Animal implements Flyable{

    public Bat(String name) {
        super(name, 2);

    }

    @Override
    public void sleep() {
        System.out.println(this.toString() + "sleep.");
    }

    @Override
    public void eat() {
        System.out.println(this.toString() + "eat.");
    }

    @Override
    public String toString() {
        return "Bird(" + this.getName() + ")";
    }

    @Override
    public void takeoff() {
        System.out.println(this.toString() + "takeoff.");
    }

    @Override
    public Void fly(){
        System.out.println(this.toString() + "fly.");
        return null;

    }
    @Override
    public void landing() {
        System.out.println(this.toString() + "landing.");
        
    }
    
}
