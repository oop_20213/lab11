package com.khunpon.week11;

public class Dog extends Animal implements Walkable,Swimable{

    public Dog(String name) {
        super(name, 4);
    }

    @Override
    public void walk() {
        System.out.println(this + "Walk.");

    }

    @Override
    public void run() {
        System.out.println(this + "run.");

    }

    @Override
    public void swim() {
        System.out.println(this + "Swim.");
        
    }

    @Override
    public void sleep() {
        System.out.println(this.toString() + "sleep.");
    }

    @Override
    public void eat() {
        System.out.println(this.toString() + "eat.");
    }

    @Override
    public String toString() {
        return "Dog(" + this.getName() + ")";
    }
    
}
